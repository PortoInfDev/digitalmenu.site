define({ "api": [
  {
    "type": "get",
    "url": "/menu/list",
    "title": "1 - Menu List",
    "version": "1.0.0",
    "group": "Menus",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "lang",
            "defaultValue": "pt",
            "description": "<p>Selected language</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Menu code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Menu name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[\n  {\n  \"code\": \"MyMenuCode1\",\n  \"name\": \"MyMenuName1\"\n  },\n  {\n  \"code\": \"MyMenuCode2\",\n  \"name\": \"MyMenuName2\"\n  }\n ]",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "http://localhost/menu/list?lang=en",
        "type": "url"
      }
    ],
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/menu/list"
      }
    ],
    "filename": "./routes/menuRouter.js",
    "groupTitle": "Menus",
    "name": "GetMenuList"
  },
  {
    "type": "post",
    "url": "/item",
    "title": "3 - Item Details",
    "version": "1.0.0",
    "group": "Menus",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "string",
            "optional": false,
            "field": "item",
            "description": "<p>Item code.</p>"
          },
          {
            "group": "Request body",
            "type": "string",
            "optional": true,
            "field": "lang",
            "defaultValue": "pt",
            "description": "<p>Selected language</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"item\": \"001\",\n  \"lang\": \"pt\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "descricao",
            "description": "<p>Item name.</p>"
          },
          {
            "group": "Success 200",
            "type": "decimal",
            "optional": false,
            "field": "precodose",
            "description": "<p>Item main price.</p>"
          },
          {
            "group": "Success 200",
            "type": "decimal",
            "optional": false,
            "field": "precomeiadose",
            "description": "<p>Item half portion price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "imprimedose",
            "description": "<p>Flag show main price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "imprimemeiadose",
            "description": "<p>Flag show halp portion price.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Item Code.</p>"
          },
          {
            "group": "Success 200",
            "optional": true,
            "field": "Array",
            "description": "<p>obs Item long description</p>"
          },
          {
            "group": "Success 200",
            "optional": true,
            "field": "String",
            "description": "<p>img Item image name (img url -&gt; http://myUrl.com/public/img/wsir/my-pic-name.jpg)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n  {\n      \"descricao\":\"Pao\",\n      \"precodose\":0.35,\n      \"precomeiadose\":0.25,\n      \"imprimedose\":1,\n      \"imprimemeiadose\":0,\n      \"codigo\":\"801\",\n      \"obs\":[\n              \"Servido com ....\",\n              \"Acompanhado com ....\"\n             ],\n      \"img\":\"my-pic-name.jpg\"\n  }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/item"
      }
    ],
    "filename": "./routes/itemRouter.js",
    "groupTitle": "Menus",
    "name": "PostItem"
  },
  {
    "type": "post",
    "url": "/menu",
    "title": "2 - Menu Details",
    "version": "1.0.0",
    "group": "Menus",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "string",
            "optional": false,
            "field": "codigo",
            "description": "<p>Menu code.</p>"
          },
          {
            "group": "Request body",
            "type": "string",
            "optional": true,
            "field": "lang",
            "defaultValue": "pt",
            "description": "<p>Selected language</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"codigo\": \"001\",\n  \"lang\": \"pt\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "menu",
            "description": "<p>Array of groups and items for selected menu.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "menu.groupName",
            "description": "<p>Group name </br> Group Img URL http://myUrl.com/public/img/wsir/My_Group.jpg </br> File Name: replace special chars and spaces with underscore.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "menu.items",
            "description": "<p>Array of items for group.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "menu.items.artigo",
            "description": "<p>Item Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "menu.items.descricao",
            "description": "<p>Item Description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "menu.items.precodose",
            "description": "<p>Item main price.</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "menu.items.imprimemeiadose",
            "description": "<p>Show half portion price (1|255-yes , 0-no).</p>"
          },
          {
            "group": "Success 200",
            "type": "Decimal",
            "optional": false,
            "field": "menu.items.precomeiadose",
            "description": "<p>Item half portion price.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n  {\n      \"menu\": [\n                  {\n                  \"groupName\": \"MyGroupName\",\n                  \"items\": [\n                              {\n                                  \"artigo\": \"ItemCode\",\n                                  \"descricao\": \"itemDescription\",\n                                  \"precodose\": 5.25,\n                                  \"imprimemeiadose\": 0,\n                                  \"precomeiadose\": 0.25\n                              } ,\n                               ....\n                              ]\n                  },\n                  ....    \n          ]\n  }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/menu"
      }
    ],
    "filename": "./routes/menuRouter.js",
    "groupTitle": "Menus",
    "name": "PostMenu"
  },
  {
    "type": "get",
    "url": "/lang",
    "title": "Languages",
    "version": "1.0.0",
    "group": "Utils",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "Lang",
            "description": "<p>ISO 639-1 codes.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n  [\n      \"en\",\n      \"fr\",\n      \"es\"\n  ]",
          "type": "Array"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/lang"
      }
    ],
    "filename": "./routes/langRouter.js",
    "groupTitle": "Utils",
    "name": "GetLang"
  },
  {
    "type": "get",
    "url": "/status",
    "title": "SQL Status",
    "version": "1.0.0",
    "group": "Utils",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "sqlStatus",
            "description": "<p>MSSQL Status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n  {\n      \"sqlStatus\": true\n  }",
          "type": "JSON"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/status"
      }
    ],
    "filename": "./routes/statusRouter.js",
    "groupTitle": "Utils",
    "name": "GetStatus"
  },
  {
    "type": "get",
    "url": "/version",
    "title": "Version",
    "version": "1.0.0",
    "group": "Utils",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "number",
            "description": "<p>Version number.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n  \"number\": \"1.x.x\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8080/api/version"
      }
    ],
    "filename": "./routes/versionRouter.js",
    "groupTitle": "Utils",
    "name": "GetVersion"
  }
] });
